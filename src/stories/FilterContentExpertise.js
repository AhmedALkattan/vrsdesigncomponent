import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import VrsSelectField from '../components/form-items/VrsSelectField';
import VrsTextField from '../components/form-items/VrsTextField';
import VrsDatePicker from '../components/form-items/VrsDatePicker';

class FilterContentExpertise extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expertiseStatusList: [
        { value: 'ocean', label: 'Ocean', isFixed: true },
        { value: 'blue', label: 'Blue', color: '#0052CC', isDisabled: true },
        { value: 'purple', label: 'Purple' },
        { value: 'red', label: 'Red', isFixed: true },
        { value: 'orange', label: 'Orange' },
        { value: 'yellow', label: 'Yellow' },
        { value: 'green', label: 'Green' },
        { value: 'forest', label: 'Forest' },
        { value: 'slate', label: 'Slate' },
        { value: 'silver', label: 'Silver' }
      ]
    };
  }

  onChange = (name) => (value) => {
    const { statePass } = this.props;
    statePass(name, value);
  };

  render() {
    const { values } = this.props;
    const { expertiseOpeningDateStart, expertiseStatus, senderInstituteCaseNo, emergencyCallNumber } = values;
    const { expertiseStatusList } = this.state;

    return (
      <React.Fragment>
        <VrsSelectField
          id="expertiseStatus"
          label={<FormattedMessage id="expertiseStatus" />}
          value={expertiseStatus}
          data={expertiseStatusList}
          onChange={this.onChange('expertiseStatus')}
          menuPortalTarget={document.body}
        />

        <VrsTextField
          id="senderInstituteCaseNo"
          label={<FormattedMessage id="senderInstituteCaseNo" />}
          value={senderInstituteCaseNo}
          onChange={this.onChange('senderInstituteCaseNo')}
        />

        <VrsTextField
          id="emergencyCallNumber"
          label={<FormattedMessage id="emergencyCallNumber" />}
          value={emergencyCallNumber}
          onChange={this.onChange('emergencyCallNumber')}
        />

        <VrsDatePicker
          id="expertiseOpeningDateStart"
          label={<FormattedMessage id="openingDateStart" />}
          value={expertiseOpeningDateStart}
          onChange={this.onChange('expertiseOpeningDateStart')}
          range
        />
      </React.Fragment>
    );
  }
}

export default FilterContentExpertise;
