import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import cls from 'classnames';
import { Folder } from '@material-ui/icons';
import styles from './DragFileSide.module.scss';

class DragFileSide extends Component {
  state = {
    drag: false
  };

  dropRef = React.createRef();

  componentDidMount() {
    const div = this.dropRef.current;
    div.addEventListener('dragenter', this.handleDragIn);
    div.addEventListener('dragleave', this.handleDragOut);
    div.addEventListener('dragover', this.handleDrag);
    div.addEventListener('drop', this.handleDrop);
  }

  // componentWillUnmount() {
  //   const div = this.dropRef.current;
  //   div.removeEventListener('dragenter', this.handleDragIn);
  //   div.removeEventListener('dragleave', this.handleDragOut);
  //   div.removeEventListener('dragover', this.handleDrag);
  //   div.removeEventListener('drop', this.handleDrop);
  // }

  handleDrag = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  handleDragIn = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.dragCounter += 1;
    if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
      this.setState({ drag: true });
    }
  };

  handleDragOut = (e) => {
    e.preventDefault();
    e.stopPropagation();
    this.dragCounter -= 1;
    if (this.dragCounter === 0) {
      this.setState({ drag: false });
    }
  };

  handleDrop = (e) => {
    const { handleDrop } = this.props;
    e.preventDefault();
    e.stopPropagation();
    this.setState({ drag: false });
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      console.log(e.dataTransfer.files);
      handleDrop(e.dataTransfer.files);
      // e.dataTransfer.clearData();
      this.dragCounter = 0;
    }
  };

  selectFile = (files) => {
    const { handleDrop } = this.props;
    handleDrop(files);
  };

  render() {
    const { drag } = this.state;
    const { accept } = this.props;
    return (
      <div className={styles.dragAndDropContainer} ref={this.dropRef}>
        <input
          onChange={(e) => {
            this.selectFile(e.target.files);
          }}
          onClick={(e) => {
            e.target.value = null;
          }}
          accept={
            accept ||
            '.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,application/vnd.ms-excel'
          }
          id="outlined-button-file"
          hidden
          type="file"
          multiple
        />

        <label
          htmlFor="outlined-button-file"
          className={cls(styles.dragAndDropBase, {
            [styles.dragAndDropBaseHover]: drag
          })}
        >
          <Folder />
          <div>
            <FormattedMessage id="dragAndDropFilesHere" />
          </div>
        </label>
      </div>
    );
  }
}
export default DragFileSide;
