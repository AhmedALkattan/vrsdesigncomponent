export const AUTH_KEY = 'userRoleAuthorizations';

const loaders = [];

export const loader = (isLoading) => {
  const loader = document.querySelector('#loader');
  loaders.push(isLoading);
  let trueCount = 0;
  let falseCount = 0;

  loaders.forEach((loader) => {
    if (loader) {
      trueCount += 1;
    } else {
      falseCount += 1;
    }
  });

  if (loader) {
    if (trueCount > falseCount) {
      loader.style.display = 'flex';
      loader.style.justifyContent = 'center';
      loader.style.alignItems = 'center';
    } else {
      loader.style.display = 'none';
    }
  }
};

export const roleAuthorizations = () => JSON.parse(sessionStorage.getItem(AUTH_KEY));

export const isAuthorized = (object, authorizationList) => {
  const authorization = authorizationList || roleAuthorizations();
  const returnValue = authorization.find(
    (item) => item.uiName === object.id || item.uiName === object.name || item.uiName === object
  );

  if (returnValue !== undefined) {
    return returnValue.isAuthorized;
  }

  return false;
};

export const authorizedFilter = (mainObj, filtObj, logicalFilter) => {
  const filtered = mainObj.filter((items) => isAuthorized(items, filtObj));
  if (logicalFilter) {
    const filteredLogical = mainObj.filter((items) => isAuthorized(items, logicalFilter));

    return filteredLogical;
  }
  return filtered;
};

export const authorizedFilterForEditMode = (editMode, menuItems, userAut, subHeader, logicalButtons) => {
  let autUnFilterData = [];
  if (editMode) {
    if (menuItems && menuItems.length > 0) {
      menuItems.forEach((dataItem) => {
        let findMenuItem;
        if (userAut && userAut.length > 0) {
          findMenuItem = userAut.find((o) => o.uiName === dataItem.name);
        }

        if (findMenuItem) {
          dataItem.isAuthorized = findMenuItem.isAuthorized;
          autUnFilterData.push(dataItem);
        } else {
          dataItem.isAuthorized = false;
          autUnFilterData.push(dataItem);
        }
      });
    }
  } else if (subHeader === 'subHeader') {
    autUnFilterData = menuItems;
  } else {
    autUnFilterData = authorizedFilter(menuItems, null, logicalButtons);
  }
  return autUnFilterData;
};

export const getCurrentLanguage = (needMap, supportedLanguages) => {
  const languageCode = JSON.parse(sessionStorage.getItem('CurrentLanguage'))
    ? JSON.parse(sessionStorage.getItem('CurrentLanguage')).CurrentLanguage
    : 'en';

  if (needMap) {
    return supportedLanguages.find((language) => language.langCode === languageCode);
  }
  return languageCode;
};
