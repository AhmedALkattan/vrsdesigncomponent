import React from 'react';
import PropTypes from 'prop-types';
import PatternLock from './PatternLock';

const VrsPatternLock = ({ path, width, size, disabled, freeze, onChange }) => {
  return (
    <PatternLock
      width={width || '100%'}
      size={size || 3}
      allowJumping
      disabled={disabled || false}
      freeze={freeze || true}
      path={path || []}
      onChange={onChange}
      allowOverlapping
    />
  );
};

VrsPatternLock.propTypes = {
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  size: PropTypes.number,
  disabled: PropTypes.bool,
  freeze: PropTypes.bool,
  path: PropTypes.arrayOf(PropTypes.number)
};

export default VrsPatternLock;
