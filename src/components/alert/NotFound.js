import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import styles from './NotFound.module.scss';
import { EmptyIcon } from '../icons';

class NotFound extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <>
        <div className={styles.notFound}>
          <EmptyIcon />
          <FormattedMessage id="noDataText" />
        </div>
      </>
    );
  }
}

export default NotFound;
