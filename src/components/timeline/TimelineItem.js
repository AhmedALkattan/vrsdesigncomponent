import React from 'react';
import cls from 'classnames';
import { StarRate, CalendarToday, CallMade, CallReceived } from '@material-ui/icons';
import styles from './TimelineItem.module.scss';

const TimelineItem = ({ title, date, icon, issuedTo, issuedFrom, operationId, isLastItem }) => {
  const createOperation = operationId === 1;
  return (
    <div className={styles.itemContainer}>
      <div className={styles.icon}>{icon || <StarRate />}</div>
      <div className={styles.item}>
        <h3
          className={cls(styles.title, {
            [styles.firstItem]: createOperation,
            [styles.success]: isLastItem
          })}
        >
          {title}
          {date && (
            <span className={styles.calendar}>
              <CalendarToday />
              {date}
            </span>
          )}
        </h3>
        {(issuedFrom || issuedTo) && (
          <div className={styles.content}>
            {issuedFrom && (
              <p className={styles.infoItem}>
                <CallReceived />
                {issuedFrom}
              </p>
            )}
            {issuedTo && (
              <p className={styles.infoItem}>
                <CallMade />
                {issuedTo}
              </p>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default TimelineItem;
