/* eslint-disable no-constant-condition */
import React, { Component } from 'react';
import cls from 'classnames';
import './Td.scss';
import Popup from '../popup/Popup';

class Td extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isPopupOpen: false,
      hasPopup: false
    };

    this.content = React.createRef();
  }

  componentDidMount() {
    this.isOverflown(this.content.current);
  }

  onMouseEvent = (type) => {
    if (type === 'over') {
      this.timer = setTimeout(() => {
        this.setState({
          isPopupOpen: true
        });
      }, 400);
    }

    if (type === 'out') {
      clearTimeout(this.timer);
      this.setState({
        isPopupOpen: false
      });
    }
  };

  isOverflown = (element) => {
    const hasScroll = element.scrollWidth > element.clientWidth;
    if (hasScroll) {
      this.setState({
        hasPopup: true
      });
    }
  };

  render() {
    const { className, children, ...otherProps } = this.props;
    const { width } = { ...otherProps.style };
    const { isPopupOpen, hasPopup } = this.state;
    return (
      <div
        className={cls('rt-td', className)}
        role="gridcell"
        {...otherProps}
        // style={{ ...style }}
        style={width ? { ...otherProps.style } : { width: '120px', flex: '120px' }}
      >
        {!hasPopup && (
          <div ref={this.content} className="td-children">
            {children}
          </div>
        )}
        {hasPopup && (
          <div
            onMouseEnter={() => this.onMouseEvent('over')}
            onMouseLeave={() => this.onMouseEvent('out')}
            className="td-children"
          >
            <span>{children}</span>
            {isPopupOpen && (
              <Popup open={isPopupOpen} className="td-popup">
                {children}
              </Popup>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default Td;
