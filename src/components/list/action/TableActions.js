/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Menu, MenuItem } from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import Popup from '../../popup/Popup';
import styles from './TableActions.module.scss';
import VrsButton from '../../buttons/Buttons';

class TableActions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null,
      islistPopupOpen: false
    };
  }

  handleClick = (event) => {
    const { actionList, row } = this.props;

    if (actionList.length > 4) {
      this.setState({ anchorEl: event.currentTarget });
    } else {
      actionList.forEach((actionItem) => {
        console.log();
        actionItem.action(row);
      });
    }
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleMouseEvents = (type, name) => {
    let whichState;
    if (name === 'description') whichState = 'islistPopupOpen';
    if (type === 'over') {
      this.timer = setTimeout(() => {
        this.setState({
          [whichState]: true
        });
      }, 100);
    }

    if (type === 'out') {
      clearTimeout(this.timer);
      this.setState({
        [whichState]: false
      });
    }
  };

  render() {
    const { actionList, row, authorizationsList } = this.props;
    const { anchorEl, islistPopupOpen } = this.state;

    return actionList.length > 4 ? (
      <>
        <VrsButton onClick={this.handleClick} className={styles.iconBox}>
          <div className={styles.topNavIcon}>
            <MoreVert />
          </div>
        </VrsButton>
        <Menu
          id={row}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          className={styles.dropdownWrapper}
        >
          {actionList.map((menuItem, i) => (
            <MenuItem key={i.toString()} onClick={() => menuItem.action(row)} className={styles.dropdownListItem}>
              <div className={styles.dropdownListIcon}>{menuItem.icon}</div>
              <FormattedMessage id={menuItem.title} />
            </MenuItem>
          ))}
        </Menu>
      </>
    ) : (
      <div className={`${styles.actionList} actionList `}>
        {actionList.map((actionItem, i) => {
          if (actionItem.icon) {
            if (actionItem.list) {
              return (
                <div
                  onMouseEnter={() => this.handleMouseEvents('over', 'description')}
                  onMouseLeave={() => this.handleMouseEvents('out', 'description')}
                >
                  <Popup
                    open={islistPopupOpen && actionItem.list(row) && actionItem.list(row).length > 0}
                    placement="right"
                  >
                    <div className={styles.actionButtonList}>
                      {actionItem.list(row).map((item) => {
                        return <div className={styles.actionButtonListItem}>{item}</div>;
                      })}
                    </div>
                  </Popup>
                  <div className={styles.iconContainer}>
                    <VrsButton
                      onClick={() => console.log()}
                      className={styles.actionButtonMore}
                      name={actionItem.name}
                      authorize={actionItem.authorize}
                      {...actionItem}
                      onlyIcon
                      xs
                      grey
                      noBg={!actionItem.noBg}
                      icon={undefined}
                      authorizationsList={authorizationsList}
                      disabled={
                        actionItem?.buttonDisabled && typeof actionItem?.buttonDisabled !== 'object'
                          ? actionItem?.buttonDisabled(row)
                          : actionItem?.buttonDisabled
                      }
                    >
                      {typeof actionItem.icon !== 'object' ? actionItem.icon(row) : actionItem.icon}
                    </VrsButton>
                  </div>
                </div>
              );
            }
            return (
              <div className={`${styles.buttonContainer} buttonContainer`} key={i.toString()}>
                <VrsButton
                  onClick={() => actionItem.action(row)}
                  toolTipName={
                    typeof actionItem.title !== 'string' ? (
                      actionItem.title(row)
                    ) : (
                      <FormattedMessage id={actionItem.title} />
                    )
                  }
                  className={styles.actionButtonMore}
                  name={actionItem.name}
                  authorize={actionItem.authorize}
                  {...actionItem}
                  title={null}
                  action={null}
                  onlyIcon
                  xs
                  grey
                  noBg={!actionItem.noBg}
                  icon={undefined}
                  authorizationsList={authorizationsList}
                  disabled={
                    actionItem?.buttonDisabled && typeof actionItem?.buttonDisabled !== 'object'
                      ? actionItem?.buttonDisabled(row)
                      : actionItem?.buttonDisabled
                  }
                >
                  {actionItem.icon && typeof actionItem.icon !== 'object' ? actionItem.icon(row) : actionItem.icon}
                </VrsButton>
              </div>
            );
          }

          return null;
        })}
      </div>
    );
  }
}

export default TableActions;
