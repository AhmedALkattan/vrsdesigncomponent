import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import styles from './TableActions.module.scss';

class TableButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showTooltip: false
    };

    this.button = React.createRef();
    this.tooltip = React.createRef();
  }

  handleHover = (e, show) => {
    this.setState({
      showTooltip: show
    });

    const elementPositions = this.button.current.getBoundingClientRect();
    this.setPositions(elementPositions);
  };

  setPositions = ({ top, left }) => {
    this.tooltip.current.style.top = `${top}px`;
    this.tooltip.current.style.left = `${left}px`;
  };

  render() {
    const { isDisabled, children, title, onClick, ...otherProps } = this.props;
    const { showTooltip } = this.state;
    return (
      <React.Fragment>
        <button
          ref={this.button}
          type="button"
          className={`${styles.actionButton} actionButton `}
          disabled={isDisabled}
          onMouseEnter={(e) => this.handleHover(e, true)}
          onMouseLeave={(e) => this.handleHover(e, false)}
          onClick={onClick}
          {...otherProps}
        >
          {children}
        </button>
        <div ref={this.tooltip} className={styles.tooltip} style={{ display: showTooltip ? 'flex' : 'none' }}>
          {title ? <FormattedMessage id={title} /> : null}
        </div>
      </React.Fragment>
    );
  }
}

export default TableButton;
