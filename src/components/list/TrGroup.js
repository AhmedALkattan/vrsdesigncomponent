import React, { Component } from 'react';
import cls from 'classnames';
import Tooltip from '@material-ui/core/Tooltip';
import './TrGroup.scss';

class TrGroup extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { children, className, data, dataChanged, ...rest } = this.props;
    const dataIsSuitable =
      data !== undefined &&
      data.reasonForNonSuitability !== undefined &&
      !data.isSuitableForFilteringOperation &&
      data.reasonForNonSuitability !== null;
    if (dataIsSuitable) {
      return (
        <Tooltip arrow title={data.reasonForNonSuitability}>
          <div className={cls('rt-tr-group', className)} role="rowgroup" {...rest}>
            {children}
          </div>
        </Tooltip>
      );
    }
    return (
      <div className={cls('rt-tr-group', className)} role="rowgroup" {...rest}>
        {children}
      </div>
    );
  }
}

export default TrGroup;
