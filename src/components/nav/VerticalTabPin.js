import React from 'react';
import { FormattedMessage } from 'react-intl';
import cls from 'classnames';
import { Tooltip } from '@material-ui/core';
import styles from './VerticalTabPin.module.scss';

const VerticalTabPin = (props) => {
  const { children, onClick, disabled, disabledText } = props;
  const text = <FormattedMessage id={disabledText || ''} />;
  return (
    <Tooltip disableHoverListener={!disabled} title={text}>
      <button
        type="button"
        onClick={onClick}
        className={cls(styles.verticalTabButton, { [styles.disabled]: disabled })}
      >
        {children}
      </button>
    </Tooltip>
  );
};

export default VerticalTabPin;
