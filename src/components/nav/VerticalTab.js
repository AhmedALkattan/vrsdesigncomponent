import React from 'react';
import styles from './VerticalTab.module.scss';

const VerticalTab = ({ children }) => {
  const { vtContainer } = styles;
  return <div className={vtContainer}>{children}</div>;
};

export default VerticalTab;
