import React, { Component } from 'react';
import VrsTextField from '../form-items/VrsTextField';

class OptionInput extends Component {
  constructor(props) {
    super(props);
    this.state = { valueState: '' };
  }

  componentDidMount() {
    const { value } = this.props;
    this.setState({
      valueState: value || '0'
    });
  }

  onChange = (name) => (value) => {
    const { isNumericOnchange, item } = this.props;
    this.setState({
      [name]: value
    });
    isNumericOnchange(value, item);
  };

  render() {
    const { disabled } = this.props;
    const { valueState } = this.state;
    return (
      <React.Fragment>
        <VrsTextField
          id="value"
          disabled={disabled}
          value={valueState}
          onChange={this.onChange('valueState')}
          isNumber
          margin="none"
          centered
        />
      </React.Fragment>
    );
  }
}

export default OptionInput;
