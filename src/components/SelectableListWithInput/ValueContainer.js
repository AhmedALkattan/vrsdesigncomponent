import React, { Component } from 'react';
import cls from 'classnames';
import { Cancel } from '@material-ui/icons';
import styles from './SelectableListWithInput.module.scss';
// import OptionInput from './OptionInput';

class Options extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  clearItem = (item) => {
    const { clearItem } = this.props;
    clearItem(item);
  };

  render() {
    const { selectedValues, isMulti, isNumeric } = this.props;

    return (
      <div className={styles.valueSideOverflew}>
        {selectedValues &&
          selectedValues !== '' &&
          selectedValues.map((valueItem) => {
            return (
              <span className={cls({ [styles.valueItem]: isMulti })}>
                <button>
                  {valueItem.label}
                  {isNumeric && <>- {valueItem.numericalValue} </>}
                </button>
                {isMulti && (
                  <button onClick={() => this.clearItem(valueItem)} className={styles.clearItem}>
                    <Cancel />
                  </button>
                )}
              </span>
            );
          })}
      </div>
    );
  }
}

export default Options;
