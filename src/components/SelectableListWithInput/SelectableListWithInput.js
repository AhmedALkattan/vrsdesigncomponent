import React, { Component } from 'react';
import { KeyboardArrowDown, Close } from '@material-ui/icons';
import cls from 'classnames';
import styles from './SelectableListWithInput.module.scss';
import Options from './Options';
import ValueContainer from './ValueContainer';

class SelectableListWithInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stateOpen: false
    };

    this.options = React.createRef();
    this.selectSide = React.createRef();
  }

  componentDidMount() {
    // gelen value varsa
    const { isList } = this.props;
    this.dataGetState();
    if (isList) {
      this.setState({
        stateOpen: true
      });
    }
  }

  dataGetState = () => {
    const { value, isMulti } = this.props;
    if (isMulti) {
      this.setState({
        selectedValues: value
      });
    } else {
      const selectedValues = [];
      selectedValues.push(value);
      this.setState({
        selectedValues
      });
    }
  };

  onClick = () => {
    const { onClick } = this.props;
    this.setState((oldState) => {
      this.closeOption(!oldState.stateOpen);
      return {
        stateOpen: !oldState.stateOpen
      };
    });

    document.addEventListener('click', this.handleOutsideClick);
    if (onClick) onClick();
  };

  handleOutsideClick = (e) => {
    if (
      this.options.current &&
      !this.options.current.contains(e.target) &&
      !this.selectSide.current.contains(e.target)
    ) {
      this.setState({
        stateOpen: false
      });

      this.closeOption(false);

      document.removeEventListener('click', this.handleOutsideClick);
    }
  };

  closeOption = (value) => {
    const { closeOption } = this.props;

    if (closeOption) {
      closeOption(value);
    }
  };

  onChange = (value) => {
    const { isMulti } = this.props;
    const { selectedValues } = this.state;

    if (isMulti) {
      let selectedValue = selectedValues || [];
      const haveValue = selectedValue.find((o) => o.id === value.id);

      if (haveValue) {
        selectedValue = selectedValue.filter((o) => o.id !== value.id);
      } else {
        selectedValue.push(value);
      }
      this.setState({
        selectedValues: selectedValue
      });
      this.selectedValueChange(selectedValue);
    } else {
      const selectedValues = [];
      if (value) selectedValues.push(value);
      this.setState({
        selectedValues
      });
      this.selectedValueChange(value);
    }
  };

  selectedValueChange = (data) => {
    const { onChange } = this.props;
    if (onChange) {
      if (data) {
        onChange(data);
      } else {
        onChange('');
      }
    }
  };

  isNumericOnchange = (value, item) => {
    const { selectedValues } = this.state;
    const dataUpdate = [];
    selectedValues.forEach((items) => {
      if (items.id === item.id) {
        const row = {
          ...items,
          numericalValue: value
        };
        dataUpdate.push(row);
      } else {
        dataUpdate.push(items);
      }
    });

    this.setState({
      selectedValues: dataUpdate
    });
    this.selectedValueChange(dataUpdate);
  };

  clearItem = (item) => {
    const { selectedValues } = this.state;

    const filterValue = selectedValues.filter((o) => o.id !== item.id);
    this.setState({
      selectedValues: filterValue
    });
    this.selectedValueChange(filterValue);
  };

  render() {
    const { value, label, data, isNumeric, isMulti, isList } = this.props;
    const { stateOpen, selectedValues } = this.state;

    return (
      <React.Fragment>
        <div className={styles.selectFieldContainer}>
          {!isList ? (
            <div
              className={cls(
                styles.selectSide,
                { [styles.selectSideHover]: stateOpen },
                { [styles.selectSideSelected]: selectedValues && selectedValues.length > 0 }
              )}
            >
              <button className={styles.values} onClick={this.onClick} ref={this.selectSide}>
                <span className={styles.labelSide}>{label}</span>
                <span className={styles.valueSide}>
                  <ValueContainer
                    isMulti={isMulti}
                    selectedValues={selectedValues}
                    clearItem={(item) => this.clearItem(item)}
                    isNumeric={isNumeric}
                  />
                </span>
              </button>
              <span className={styles.iconSide}>
                {value && (
                  <button className={styles.clearButton} onClick={() => this.onChange()}>
                    <Close />
                  </button>
                )}
                <button onClick={this.onClick} ref={this.selectSide}>
                  <KeyboardArrowDown />
                </button>
              </span>
            </div>
          ) : (
            <div className={styles.listTitle}>{label}</div>
          )}

          {stateOpen && (
            <div className={cls(styles.options, { [styles.optionsList]: isList })} ref={this.options}>
              {data &&
                data.map((item) => {
                  return (
                    <Options
                      item={item}
                      selectedValues={selectedValues}
                      closeOption={(value) => this.closeOption(value)}
                      onChange={(item) => this.onChange(item)}
                      isNumericOnchange={(value, item) => this.isNumericOnchange(value, item)}
                      isNumeric={isNumeric}
                      isList={isList}
                    />
                  );
                })}
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default SelectableListWithInput;
