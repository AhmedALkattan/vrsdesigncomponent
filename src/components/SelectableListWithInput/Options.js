import React, { Component } from 'react';
import cls from 'classnames';
import { Check } from '@material-ui/icons';
import styles from './SelectableListWithInput.module.scss';
import OptionInput from './OptionInput';
import VrsCheckbox from '../form-items/VrsCheckbox';

class Options extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  closeOption = (value) => {
    const { closeOption } = this.props;
    closeOption(value);
  };

  onChange = (item) => {
    const { onChange } = this.props;
    onChange(item);
  };

  isNumericOnchange = (value, item) => {
    const { isNumericOnchange } = this.props;
    isNumericOnchange(value, item);
  };

  render() {
    const { selectedValues, item, isNumeric, isList } = this.props;

    let isSelected;
    if (selectedValues && selectedValues.length !== 0) {
      isSelected = selectedValues.find((o) => o.id === item.id);
    } else {
      isSelected = undefined;
    }

    return (
      <React.Fragment>
        <div className={cls(styles.optionsItem, { [styles.optionsItemHover]: isSelected })}>
          <button className={styles.optionsButton} onClick={() => this.closeOption(false)}>
            <span />
            <button className={styles.textSide} onClick={() => this.onChange(item)}>
              <span className={styles.checkboxContainer}>
                <VrsCheckbox value={isSelected} isRight className={styles.customCheckbox} />
              </span>
              <span className={styles.Optionlabel}>{item.label}</span>
            </button>
          </button>

          <div className={styles.iconSide}>
            {isSelected && !isList && (
              <span className={styles.isSelected}>
                <Check />
              </span>
            )}
          </div>

          {isNumeric && (
            <div className={styles.inputSide}>
              <OptionInput
                isNumericOnchange={(value, item) => this.isNumericOnchange(value, item)}
                item={item}
                value={isSelected && isSelected.numericalValue}
                disabled={!isSelected}
              />
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default Options;
