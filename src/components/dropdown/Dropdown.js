/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import cls from 'classnames';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import styles from './Dropdown.module.scss';

class Dropdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stateOpen: false
    };

    this.dropdown = React.createRef();
    this.button = React.createRef();
  }

  componentDidUpdate(prevProps) {
    const { closeDropdownState } = this.props;
    if (prevProps.closeDropdownState && prevProps.closeDropdownState !== closeDropdownState) {
      this.setState({
        stateOpen: closeDropdownState
      });
    }
  }

  onClick = () => {
    const { onClick } = this.props;
    this.setState((oldState) => {
      this.closeDropdown(!oldState.stateOpen);
      return {
        stateOpen: !oldState.stateOpen
      };
    });

    document.addEventListener('click', this.handleOutsideClick);
    if (onClick) onClick();
  };

  handleOutsideClick = (e) => {
    if (this.dropdown.current && !this.dropdown.current.contains(e.target) && !this.button.current.contains(e.target)) {
      this.setState({
        stateOpen: false
      });
      this.closeDropdown(false);

      document.removeEventListener('click', this.handleOutsideClick);
    }
  };

  closeDropdown = (value) => {
    const { closeDropdown } = this.props;
    if (closeDropdown) {
      closeDropdown(value);
    }
  };

  render() {
    const { children, className, image, icon, hideArrow, open, buttonContent } = this.props;
    const { stateOpen } = this.state;
    const { dropdownContainer, button, iconContainer, dropdown, active, content } = styles;
    const Icon = icon;
    return (
      <div className={cls(dropdownContainer, className)}>
        {icon && (
          <button className={button} onClick={this.onClick} ref={this.button}>
            <div className={iconContainer}>
              {image && <Avatar src={image} />}
              {!image && <Icon />}
            </div>
            <div className={content}>{buttonContent && buttonContent}</div>
            {!hideArrow && <KeyboardArrowDown className={cls({ [active]: stateOpen })} />}
          </button>
        )}
        {(stateOpen || open) && (
          <div className={dropdown} ref={this.dropdown} onClick={() => this.closeDropdown(false)}>
            {children}
          </div>
        )}
      </div>
    );
  }
}

export default Dropdown;
