import React from 'react';
import cls from 'classnames';
import styles from './DropdownMenu.module.scss';

const DropdownMenu = ({ children, className, ...otherProps }) => {
  const { dropdownMenu } = styles;
  return (
    <ul className={cls(dropdownMenu, className)} {...otherProps}>
      {children}
    </ul>
  );
};

export default DropdownMenu;
