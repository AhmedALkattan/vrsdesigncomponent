/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import VrsCheckbox from '../form-items/VrsCheckbox';
import VrsTextField from '../form-items/VrsTextField';
import VrsSelectField from '../form-items/VrsSelectField';
import VrsDatePicker from '../form-items/VrsDatePicker';
import styles from './AccordionTableForm.module.scss';

class AccordionTableForm extends Component {
  constructor(props) {
    super(props);
    const { name, defaultValue, isSemiControlledInput, values } = this.props;
    this.state = {
      [name]: isSemiControlledInput ? defaultValue : values
    };
  }

  onChange = (name) => (value) => {
    const { statePass, row } = this.props;

    this.setState({ [name]: value });
    if (statePass) {
      statePass(name, value, row);
    }
  };

  render() {
    const {
      name,
      data,
      isMulti,
      label,
      type,
      isSemiControlledInput,
      hasCustomColoredText,
      customColor,
      ...otherProps
    } = this.props;
    return (
      <div className={styles.formGroup}>
        {type === 'select' && (
          <VrsSelectField
            id={name}
            label={label && <FormattedMessage id={label} />}
            value={this.state[name]}
            data={data}
            onChange={this.onChange(name)}
            isMulti={isMulti || false}
            margin="none"
            {...otherProps}
          />
        )}
        {type === 'text' && (
          <VrsTextField
            id={name}
            label={label && <FormattedMessage id={label} />}
            value={this.state[name]}
            defaultValue={isSemiControlledInput ? this.state[name] : null}
            onChange={this.onChange(name)}
            margin="none"
            hasCustomColoredText={hasCustomColoredText}
            customColor={customColor}
            {...otherProps}
          />
        )}
        {type === 'date' && (
          <VrsDatePicker
            id={name}
            label={label && <FormattedMessage id={label} />}
            value={this.state[name]}
            onChange={this.onChange(name)}
            margin="none"
            {...otherProps}
          />
        )}
        {type === 'checbox' && (
          <VrsCheckbox
            id={name}
            label={label && <FormattedMessage id={label} />}
            value={this.state[name]}
            onChange={this.onChange(name)}
            {...otherProps}
          />
        )}
      </div>
    );
  }
}

export default AccordionTableForm;
