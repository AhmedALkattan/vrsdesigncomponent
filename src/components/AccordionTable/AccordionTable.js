/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import cls from 'classnames';
import AccordionTableHead from './AccordionTableHead';
import AccordionTableBody from './AccordionTableBody';
import styles from './AccordionTable.module.scss';

class AccordionTable extends Component {
  constructor(props) {
    super(props);
    // conthis.props
    this.state = {
      totalHeight: ''
    };
  }

  // selected checkbox

  componentDidMount() {
    const { selectedItems } = this.props;
    if (selectedItems && selectedItems.length > 0) {
      this.setState({
        selectedItems
      });
    }
    this.canvasHeighter();
    window.addEventListener('resize', () => {
      this.canvasHeighter();
    });
  }

  componentDidUpdate = (prevProp) => {
    const { selectedItems, data, onChangeTableCheckboxGroup, keyField } = this.props;
    if (prevProp.selectedItems !== selectedItems) {
      const selection = selectedItems;
      if (onChangeTableCheckboxGroup) {
        const filterList = selection.filter((o1) => {
          return data.some((o2) => {
            return o1 === o2[keyField];
          });
        });
        this.setState({
          selectedItems,
          allSelected: filterList.length === data.length
        });
      } else {
        this.setState({
          selectedItems,
          allSelected: selectedItems.length === data.length
        });
      }
    }

    if (prevProp.data !== data && data.length === 0) {
      this.setState({
        selectedItems: [],
        allSelected: false
      });
    }

    if (prevProp.data !== data) {
      this.canvasHeighter();
    }
  };

  componentWillUnmount() {
    window.removeEventListener('resize', () => {
      this.canvasHeighter();
    });
  }

  handleChange = (panel, isExpanded) => {
    const { handleChange } = this.props;
    if (handleChange) {
      handleChange(panel, isExpanded);
    }
  };

  setSelectedRow = (row) => {
    const { setSelectedRow } = this.props;
    if (setSelectedRow) {
      setSelectedRow(row);
    }
  };

  onRowDblClick = (row) => {
    const { onRowDblClick } = this.props;
    if (onRowDblClick) {
      onRowDblClick(row);
    }
  };

  // table element Change
  // sayfa içinde birden fazla tablo olduğunda sıkıntı olabiliyor.
  // Onun için buradan yönetip hem üst componente gönderiyoruz hem alta (body'ye)
  onChangeTableCheckbox = (name, value) => {
    const { onChangeTableCheckbox, data, onChangeTableCheckboxGroup, keyField } = this.props;
    const { selectedItems } = this.state;

    if (onChangeTableCheckboxGroup) {
      if (value) {
        const selectedItemsDataUpdate = selectedItems && selectedItems.length > 0 ? selectedItems : [];

        selectedItemsDataUpdate.push(name);
        const selectedItemsDataLeng = selectedItemsDataUpdate.filter((o1) => {
          return data.some((o2) => {
            return o1 === o2[keyField];
          });
        });
        this.setState({
          allSelected: selectedItemsDataLeng.length === data.length
        });
      } else {
        this.setState({
          allSelected: false
        });
      }

      onChangeTableCheckbox(name, value);
    } else {
      const selectedItemsDataUpdate = selectedItems && selectedItems.length > 0 ? selectedItems : [];
      if (value) {
        selectedItemsDataUpdate.push(name);
        console.log('selectedItemsDataUpdate.length', selectedItemsDataUpdate.length);

        this.setState({
          selectedItems: selectedItemsDataUpdate,
          allSelected: selectedItemsDataUpdate.length === data.length
        });
        onChangeTableCheckbox(selectedItemsDataUpdate, data);
      } else {
        const findItem = selectedItems.find((o) => o === name);
        const filterItem = selectedItems.filter((o) => o !== findItem);
        this.setState({
          selectedItems: filterItem,
          allSelected: false
        });
        onChangeTableCheckbox(filterItem, data);
      }
    }
  };

  // selected all checkbox // sayfa içinde birden fazla tablo olduğunda sıkıntı olabiliyor.
  // Onun için buradan yönetip hem üst componente gönderiyoruz hem alta (body'ye)
  listAllSelected = (name, value) => {
    const { listAllSelected, data, keyField, selectedItems } = this.props;
    let selectedItemsData = selectedItems || [];
    this.setState({
      allSelected: value
    });

    if (value) {
      data.forEach((item) => {
        let findSelected = null;
        if (selectedItems) {
          findSelected = selectedItems.find((o) => o === item.id);
        }
        if (!findSelected) {
          selectedItemsData.push(item[keyField]);
        }
      });

      this.setState({
        selectedItems: selectedItemsData
      });
    } else {
      selectedItemsData = selectedItems.filter((o1) => {
        return !data.some((o2) => {
          return o1 === o2[keyField];
        });
      });

      this.setState({
        selectedItems: selectedItemsData
      });
    }
    listAllSelected(name, value, selectedItemsData);
  };

  acordionDetailAddNewItem = (id, item) => {
    const { acordionDetailAddNewItem } = this.props;
    if (acordionDetailAddNewItem) {
      acordionDetailAddNewItem(id, item);
    }
  };

  listOrder = (data) => {
    const { listOrder } = this.props;
    if (listOrder) {
      listOrder(data);
    }
  };

  handleSearch = (e) => {
    const { handleSearch } = this.props;
    const { expanded } = this.state;
    handleSearch(e, expanded);
  };

  detailItemListFormDataChange = (name, value, row) => {
    const { detailItemListFormDataChange } = this.props;
    detailItemListFormDataChange(name, value, row);
  };

  canvasHeighter = () => {
    const { heighters, disableHeighters, pagePadding } = this.props;
    if (!disableHeighters) {
      const header = document.querySelector('header');
      const subheader = document.querySelector('.subheader');
      let sumOtherHeight = 0;

      if (heighters) {
        heighters.map((heighter) => {
          if (heighter !== null) {
            if (heighter.current) {
              sumOtherHeight += heighter.current.clientHeight;
            } else {
              sumOtherHeight += heighter.clientHeight;
            }
          }
          return sumOtherHeight;
        });
      }

      if (header && subheader) {
        const headerHeight = header.clientHeight;
        const subheaderHeight = subheader.clientHeight;
        const pagePaddings = pagePadding || 0;
        // buradaki +2 sub header içindeki border için :)
        const totalHeight = window.innerHeight - (subheaderHeight + headerHeight + sumOtherHeight + 2) - pagePaddings;

        this.setState({
          totalHeight:
            window.innerHeight >= 640
              ? parseInt(totalHeight, 10)
              : parseInt(totalHeight + headerHeight + subheaderHeight, 10)
        });
      }
    }
  };

  render() {
    const {
      columns,
      data,
      actionList,
      checkbox,
      accordion,
      subListData,
      stylesTable,
      selectedRow,
      accordionDetailAllItem,
      // sil
      allButtonListFilter,
      dragged,
      keyField,
      allExpanded,
      galleryView,
      onChangeTableCheckboxGroup,
      SubComponent,
      allButtonListAut,
      allSelectedFalse,
      disabled,
      galleryViewCustom,
      grids,
      disabledRow,
      expanted,
      heighters,
      loading,
      subComponentCheckboxStatus,
      pagePadding,
      shouldFilterActionsFromService,
      isTotalHeight
    } = this.props;

    const pagePaddings = pagePadding || 0;

    const { allSelected, selectedItems, totalHeight } = this.state;

    return (
      <div
        className={cls(styles.accordionTableContent, {
          [styles.headerStyles]: stylesTable === 'headerStyles',
          headeTop: stylesTable === 'headerStyles' && grids && subComponentCheckboxStatus,
          [styles.resetStyles]: stylesTable === 'reset',
          [styles.accordionWrapperMini]: stylesTable === 'mini'
        })}
        style={{
          display: 'flex',
          flexDirection: 'column',
          overflow: 'auto',
          height: isTotalHeight && totalHeight ? `${totalHeight}px` : '100%'
        }}
      >
        <AccordionTableHead
          columns={columns}
          checkbox={checkbox}
          onChangeTableCheckboxGroup={onChangeTableCheckboxGroup}
          listAllSelected={(name, value) => this.listAllSelected(name, value)}
          galleryView={galleryView}
          allSelected={allSelected}
          allSelectedFalse={allSelectedFalse}
          disabled={disabled}
          stylesTable={stylesTable}
        />
        <AccordionTableBody
          columns={columns}
          data={data}
          actionList={actionList}
          checkbox={checkbox}
          allSelected={allSelected}
          selectedItems={selectedItems}
          listAllSelected={(name, value) => this.listAllSelected(name, value)}
          onChangeTableCheckbox={(name, value) => this.onChangeTableCheckbox(name, value)}
          handleChange={(panel, isExpanded) => this.handleChange(panel, isExpanded)}
          handleSearch={(e, expanded) => this.handleSearch(e, expanded)}
          setSelectedRow={(row) => this.setSelectedRow(row)}
          onRowDblClick={(row) => this.onRowDblClick(row)}
          accordion={accordion}
          subListData={subListData}
          stylesTable={stylesTable}
          selectedRow={selectedRow}
          allButtonListFilter={allButtonListFilter}
          accordionDetailAllItem={accordionDetailAllItem}
          acordionDetailAddNewItem={(id, item) => this.acordionDetailAddNewItem(id, item)}
          detailItemListFormDataChange={(name, value, row) => this.detailItemListFormDataChange(name, value, row)}
          listOrder={(data) => this.listOrder(data)}
          dragged={dragged}
          keyField={keyField}
          allExpanded={allExpanded}
          galleryView={galleryView}
          SubComponent={SubComponent}
          allButtonListAut={allButtonListAut}
          disabled={disabled}
          galleryViewCustom={galleryViewCustom}
          grids={grids}
          subComponentCheckboxStatus={subComponentCheckboxStatus}
          disabledRow={disabledRow}
          expanted={expanted}
          loading={loading}
          totalHeight={totalHeight - pagePaddings}
          heighters={heighters}
          shouldFilterActionsFromService={shouldFilterActionsFromService}
        />
      </div>
    );
  }
}

export default AccordionTable;
