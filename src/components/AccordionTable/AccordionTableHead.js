import React, { Component } from 'react';
import cls from 'classnames';
import { FormattedMessage } from 'react-intl';
import VrsCheckbox from '../form-items/VrsCheckbox';
import styles from './AccordionTableHead.module.scss';
import './AccordionTableHead.scss';

class AccordionTableHead extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allSelected: false
    };
  }

  componentDidUpdate(prevProps) {
    const { allSelected } = this.props;
    if (prevProps.allSelected !== allSelected) {
      this.updateAllSelectedCheckbox(allSelected);
    }
  }

  updateAllSelectedCheckbox = (value) => {
    this.setState({
      allSelected: value
    });
  };

  listAllSelected = (name) => (value) => {
    const { listAllSelected } = this.props;
    this.setState({
      allSelected: value
    });

    listAllSelected(name, value);
  };

  render() {
    const { columns, checkbox, galleryView, allSelectedFalse, disabled, stylesTable } = this.props;
    const { allSelected } = this.state;

    return (
      <React.Fragment>
        <div
          className={cls(styles.headerWrapper, {
            [styles.headerWrapperGalleryView]: galleryView && galleryView.visibility,
            [styles.headerStyles]: stylesTable === 'headerStyles',
            [styles.accordionWrapperMini]: stylesTable === 'mini'
          })}
        >
          {checkbox && (
            <div
              className={`headerCheckbox ${styles.headerItem}`}
              style={galleryView && galleryView.visibility ? { width: 'auto' } : { width: '50px' }}
            >
              {!allSelectedFalse && (
                <div className={` ${styles.checkbox}`}>
                  <VrsCheckbox
                    isRight
                    checked={allSelected}
                    value={allSelected}
                    onChange={this.listAllSelected('allSelected')}
                    disabled={disabled}
                  />
                </div>
              )}
            </div>
          )}

          {!galleryView &&
            columns &&
            columns.map((item, i) => {
              return (
                <>
                  {item.isVisible && (
                    <div
                      key={i.toString()}
                      className={cls(styles.headerItem, { [styles.headerItemCenter]: item.alignmentIsMiddle })}
                      style={
                        item.width !== 'auto'
                          ? {
                              width: `${item.width}px`,
                              flex: `${item.width} 0 auto`,
                              maxWidth: `${item.width}px`
                            }
                          : { width: `120px`, flex: `1 1 120px` }
                      }
                    >
                      {item.columnCaption && (
                        <FormattedMessage defaultMessage={item.columnCaption} id={item.columnCaption} />
                      )}
                    </div>
                  )}
                </>
              );
            })}
          {galleryView &&
            !galleryView.visibility &&
            columns.map((item, i) => {
              return (
                <div key={i.toString()}>
                  {item.isVisible && (
                    <div
                      className={cls(styles.headerItem, { [styles.headerItemCenter]: item.alignmentIsMiddle })}
                      style={
                        item.width !== 'auto'
                          ? {
                              width: `${item.width}px`,
                              flex: `${item.width} 0 auto`,
                              maxWidth: `${item.width}px`
                            }
                          : { width: `120px`, flex: `1 1 120px` }
                      }
                    >
                      {item.columnCaption && (
                        <FormattedMessage defaultMessage={item.columnCaption} id={item.columnCaption} />
                      )}
                    </div>
                  )}
                </div>
              );
            })}
        </div>
      </React.Fragment>
    );
  }
}

export default AccordionTableHead;
