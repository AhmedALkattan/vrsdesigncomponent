/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { AccordionSummary, Accordion, AccordionDetails, Grid } from '@material-ui/core';
import { Add, ExpandMore } from '@material-ui/icons';
import cls from 'classnames';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import VrsButton from '../buttons/Buttons';
import VrsCheckbox from '../form-items/VrsCheckbox';
import styles from './AccordionTableBody.module.scss';
import AccordionTableForm from './AccordionTableForm';
import './acordion.scss';
import { isAuthorized } from '../util/Util';
import Loader from '../loader/Loader';
import NotFound from '../alert/NotFound';

class AccordionTableBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      listData: [],
      anchorEl: null
    };
    this.onDragEnd = this.onDragEnd.bind(this);
  }

  componentDidMount() {
    const { data } = this.props;
    // list data for state
    this.setState({ listData: data });
  }

  componentDidUpdate(prevProps) {
    const { allSelected, selectedItems, data, keyField, expanted } = this.props;
    const { listData } = this.state;

    // selected durumu için
    if (prevProps.allSelected !== allSelected && allSelected && selectedItems) {
      selectedItems.forEach((items) => {
        this.setState({
          [items]: true
        });
      });
    }
    // yukarının else i
    if (
      prevProps.allSelected !== allSelected &&
      allSelected === false &&
      selectedItems &&
      selectedItems.length !== 0 &&
      prevProps.selectedItems
    ) {
      prevProps.selectedItems.forEach((items) => {
        this.setState({
          [items]: false
        });
      });
    }

    if (prevProps.allSelected !== allSelected) {
      this.updateAllSelectedCheckbox(allSelected);
    }

    if (prevProps.selectedItems !== selectedItems) {
      // data.forEach((row) => {
      //   this.setState({
      //     [row[keyField]]: false
      //   });
      // });
      prevProps.data.forEach((row) => {
        this.setState({
          [row[keyField]]: false
        });
      });
      if (selectedItems && selectedItems.length > 0) {
        selectedItems.forEach((item) => {
          this.setState({
            [item]: true
          });
        });
      }
    }

    if (prevProps.expanted !== expanted) {
      this.setState({
        expanded: expanted
      });
    }
    if (prevProps.data !== listData) {
      this.setState({ listData: data });
    }
  }

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    this.reorder(this.state.listData, result.source.index, result.destination.index);
  }

  // a little function to help us with reordering the result
  reorder = (list, startIndex, endIndex) => {
    const { expanded } = this.state;
    const { listOrder } = this.props;
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    this.setState({
      listData: result
    });
    if (listOrder) {
      this.handleChange(expanded)(false, false);
      listOrder(result);
    }
  };

  getItemStyle = (isDragging, draggableStyle) => ({
    // change background colour if dragging
    background: isDragging ? 'lightgreen' : '',

    // styles we need to apply on draggables
    ...draggableStyle
  });

  getListStyle = (isDraggingOver) => ({
    background: isDraggingOver ? 'lightblue' : ''
  });

  handleChange = (row) => (event, isExpanded) => {
    const { handleChange, keyField } = this.props;
    const { accordion } = this.props;

    if (accordion) {
      if (isExpanded) {
        this.setState({ expanded: row[keyField] });
        this.setSelectedRow(row);
      } else {
        this.setState({ expanded: false });
      }
      // if (handleChange) {
      handleChange(row[keyField], isExpanded);
      // }
    }
  };

  handleClick = (row, title) => {
    const { actionList } = this.props;
    let findButton;
    if (row.rowActions) {
      findButton = row.rowActions.find((o) => o.title === title);
    } else {
      findButton = actionList.find((o) => o.title === title);
    }

    findButton.action(row, title);
  };

  handleClickAction = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  onChangeTableCheckbox = (name) => (value) => {
    const { onChangeTableCheckbox } = this.props;
    this.setState({
      [name]: value
    });
    onChangeTableCheckbox(name, value);
  };

  setSelectedRow = (row) => {
    const { setSelectedRow } = this.props;
    if (setSelectedRow) {
      setSelectedRow(row);
    }
  };

  onRowDblClick = (row) => {
    const { onRowDblClick } = this.props;
    if (onRowDblClick) {
      onRowDblClick(row);
    }
  };

  acordionDetailAddNewItem = (id, item) => {
    const { acordionDetailAddNewItem } = this.props;
    if (acordionDetailAddNewItem) {
      acordionDetailAddNewItem(id, item);
    }
  };

  handleSearch = (e) => {
    const { handleSearch } = this.props;
    const { expanded } = this.state;
    handleSearch(e, expanded);
  };

  statePass = (name, value, row) => {
    const { detailItemListFormDataChange } = this.props;
    detailItemListFormDataChange(name, value, row);
  };

  listAllSelected = (name) => (value) => {
    const { listAllSelected } = this.props;
    this.setState({
      allSelected: value
    });

    listAllSelected(name, value);
  };

  updateAllSelectedCheckbox = (value) => {
    this.setState({
      allSelected: value
    });
  };

  render() {
    const {
      columns,
      accordion,
      actionList,
      checkbox,
      stylesTable,
      selectedRow,
      allButtonListFilter,
      data,
      dragged,
      keyField,
      allExpanded,
      galleryView,
      SubComponent,
      allButtonListAut,
      intl,
      disabled,
      galleryViewCustom,
      grids,
      disabledRow,
      subComponentCheckboxStatus,
      loading,
      shouldFilterActionsFromService
    } = this.props;
    const { expanded } = this.state;

    const { searchCaption } = intl.messages;

    let columnsIconFind;
    let galleryLabelcolumnsIconFind;

    if (galleryView && galleryView.visibility && galleryView.galleryViewData) {
      // label list geçince
      columnsIconFind = columns.find((o) => o.correspondingField === galleryView.galleryViewData.icon);

      if (galleryView.galleryViewData.labelList && galleryView.galleryViewData.labelList.length > 0) {
        galleryView.galleryViewData.labelList.forEach((item) => {
          const galleryLabelcolumnsIconFindIn = columns.find((o) => o.correspondingField === item.name);
          if (galleryLabelcolumnsIconFindIn && galleryLabelcolumnsIconFindIn.Cell) {
            galleryLabelcolumnsIconFind = galleryLabelcolumnsIconFindIn;
          }
        });
      }
    }

    const stopPropagation = (e) => e.stopPropagation();
    const PreventPropagationWrapper = ({ children }) => (
      <div style={{ display: 'flex' }} onClick={stopPropagation}>
        {children}
      </div>
    );

    return (
      <React.Fragment>
        <div className={styles.tableBody} style={{ height: '100%' }}>
          {loading && <Loader loading={loading} reset />}
          <DragDropContext onDragEnd={this.onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided, snapshot) => (
                <div
                  className={cls(
                    styles.accordionWrapper,
                    { [styles.accordionWrapperTransparent]: stylesTable === 'transparent' },
                    { [styles.accordionWrapperStyle2]: stylesTable === 'style2' },
                    { [styles.accordionWrapperMini]: stylesTable === 'mini' },
                    { [styles.accordionWrapperBorder]: stylesTable === 'borderStyles' },
                    { [styles.accordionWrapperAllExpanded]: stylesTable === 'allExpandedStyles' },
                    { [styles.headerStyles]: stylesTable === 'headerStyles' },
                    { [styles.headerLeft]: stylesTable === 'headerLeft' },
                    { headerLeft: stylesTable === 'headerLeft' },
                    {
                      [styles.headerStylesGrid]: stylesTable === 'headerStyles' && grids && subComponentCheckboxStatus
                    },
                    { [styles.galleryView]: galleryView && galleryView.visibility },
                    { [styles.grids]: grids },
                    { [styles.disabledTable]: disabled }
                  )}
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  style={this.getListStyle(snapshot.isDraggingOver)}
                >
                  {data &&
                    data.map((row, i) => {
                      return (
                        <Draggable
                          key={row[keyField] ? row[keyField].toString() : i.toString()}
                          draggableId={row[keyField] ? row[keyField].toString() : i.toString()}
                          index={i}
                          isDragDisabled={!dragged}
                          disabled={!dragged}
                        >
                          {(provided, snapshot) => (
                            <Accordion
                              className={cls(
                                styles.accordionStyles,
                                { accordionBody: !accordion },
                                { [styles.acordionSelected]: selectedRow && selectedRow.id === row[keyField] },
                                {
                                  [styles.disabledRow]:
                                    disabledRow && !row[disabledRow.dataField] && disabledRow.isActive
                                },
                                {
                                  [styles.disabledRowShow]:
                                    disabledRow &&
                                    !row[disabledRow.dataField] &&
                                    disabledRow.isActive &&
                                    disabledRow.showFiltered
                                },
                                { rowMainFlex: stylesTable === 'headerLeft' }
                              )}
                              classes={{
                                root:
                                  (galleryView && galleryView.flex && styles[galleryView.flex]) ||
                                  (grids && styles[grids])
                              }}
                              expanded={allExpanded ? true : expanded === row[keyField]}
                              onChange={this.handleChange(row)}
                              key={i.toString()}
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              style={this.getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
                            >
                              <AccordionSummary
                                expandIcon={
                                  stylesTable === 'allExpandedStyles' ||
                                  stylesTable === 'headerStyles' ||
                                  stylesTable === 'headerLeft' ? (
                                    <></>
                                  ) : (
                                    <>
                                      {!allExpanded && ( // when the accordion is normal and not expanded show the actions with the expandMoreIcon
                                        <PreventPropagationWrapper>
                                          {actionList &&
                                            actionList.map((actionItem) => {
                                              let shouldShowAction = !shouldFilterActionsFromService;
                                              if (shouldFilterActionsFromService) {
                                                Object.keys(row).forEach((key) => {
                                                  if (
                                                    key.toLowerCase().includes(actionItem.title) &&
                                                    row[key] === true
                                                  ) {
                                                    shouldShowAction = true;
                                                  }
                                                });
                                              }
                                              if (shouldShowAction) {
                                                return (
                                                  <VrsButton
                                                    generalButtonColor
                                                    onClick={() => this.handleClick(row, actionItem.title)}
                                                    toolTipName={
                                                      typeof actionItem.title !== 'string' ? (
                                                        actionItem.title(row)
                                                      ) : (
                                                        <FormattedMessage id={actionItem.title} />
                                                      )
                                                    }
                                                    className={styles.actionButtonMore}
                                                    name={actionItem.name}
                                                    authorize={actionItem.authorize}
                                                    {...actionItem}
                                                    title={null}
                                                    action={null}
                                                    onlyIcon
                                                    small
                                                    noBg={!actionItem.noBg}
                                                    icon={undefined}
                                                  >
                                                    {typeof actionItem.icon !== 'object'
                                                      ? actionItem.icon(row)
                                                      : actionItem.icon}
                                                  </VrsButton>
                                                );
                                              }
                                              return null;
                                            })}
                                        </PreventPropagationWrapper>
                                      )}
                                      <ExpandMore className={styles.expandMoreIcon} />
                                    </>
                                  )
                                }
                                aria-label="Expand"
                                aria-controls={`aria${row[keyField]}`}
                                id="additional-actions1-header"
                                className={styles.rowSelf}
                                classes={{
                                  root: styles.rowSelfRoot,
                                  content: styles.rowSelfContent,
                                  expandIcon: styles.expandIcon
                                }}
                              >
                                <div
                                  className={styles.accordionSummary}
                                  onClick={(event) => event.stopPropagation()}
                                  onFocus={(event) => event.stopPropagation()}
                                >
                                  {checkbox && (
                                    <div
                                      className={cls(styles.rowItem, styles.rowCheckbox, {
                                        [styles.rowCheckboxGalleryView]: galleryView
                                      })}
                                      style={galleryView && galleryView.visibility ? null : { width: '50px' }}
                                    >
                                      <div className={styles.checkbox2}>
                                        <VrsCheckbox
                                          checked={this.state[row[keyField]] || false}
                                          value={this.state[row[keyField]] || false}
                                          onChange={this.onChangeTableCheckbox(row[keyField])}
                                          disabled={
                                            disabledRow &&
                                            !row[disabledRow.dataField] &&
                                            disabledRow.showFiltered &&
                                            disabledRow.isActive
                                          }
                                        />
                                      </div>
                                    </div>
                                  )}
                                  <div
                                    onClick={() => this.setSelectedRow(row)}
                                    onDoubleClick={() => this.onRowDblClick(row)}
                                    className={styles.accordionSummaryData}
                                  >
                                    {galleryView && galleryView.visibility ? (
                                      <>
                                        {galleryViewCustom ? (
                                          galleryViewCustom(row)
                                        ) : (
                                          <>
                                            {galleryView.galleryViewData && (
                                              <>
                                                <div className={cls(styles.rowItem, styles.rowItemIcon)}>
                                                  {columnsIconFind && columnsIconFind.Cell && columnsIconFind.Cell(row)}
                                                </div>
                                                <div className={cls(styles.rowItem, styles.rowItemTitle)}>
                                                  {row[galleryView.galleryViewData.title]}
                                                </div>
                                                {galleryView.galleryViewData.labelList &&
                                                  galleryView.galleryViewData.labelList.map((galleryLabelItem) => {
                                                    return (
                                                      <div className={cls(styles.rowItem, styles.labelListItem)}>
                                                        {galleryLabelcolumnsIconFind &&
                                                        galleryLabelcolumnsIconFind.Cell &&
                                                        galleryLabelcolumnsIconFind.correspondingField ===
                                                          galleryLabelItem.name ? (
                                                          <span className={styles.labelListItemIcon}>
                                                            <p>
                                                              <FormattedMessage
                                                                defaultMessage={galleryLabelItem.name}
                                                                id={galleryLabelItem.name}
                                                              />
                                                              :
                                                            </p>
                                                            <span>{galleryLabelcolumnsIconFind.Cell(row)}</span>
                                                          </span>
                                                        ) : (
                                                          <>
                                                            {row[galleryLabelItem.value] && (
                                                              <>
                                                                {/* <p>
              <FormattedMessage
                defaultMessage={galleryLabelItem.name}
                id={galleryLabelItem.name}
              />
            </p> */}
                                                                <span>{row[galleryLabelItem.value]}</span>
                                                              </>
                                                            )}
                                                          </>
                                                        )}
                                                      </div>
                                                    );
                                                  })}
                                              </>
                                            )}
                                          </>
                                        )}
                                      </>
                                    ) : (
                                      <>
                                        {columns &&
                                          columns.map((item) => {
                                            return (
                                              <>
                                                {item.isVisible && (
                                                  <div
                                                    className={cls(
                                                      styles.rowItem,
                                                      { [styles.rowItemCenter]: item.alignmentIsMiddle },
                                                      { [styles.rowItemCenterEnd]: item.alignment === 'end' }
                                                    )}
                                                    style={
                                                      item.width !== 'auto'
                                                        ? {
                                                            width: `${item.width}px`,
                                                            flex: `${item.width} 0 auto`,
                                                            maxWidth: `${item.width}px`
                                                          }
                                                        : { width: `120px`, flex: `1 1 120px` }
                                                    }
                                                  >
                                                    {!row.rowEdit && item.Cell ? (
                                                      item.Cell(row)
                                                    ) : (
                                                      <>
                                                        {row[item.correspondingField] &&
                                                          !Array.isArray(row[item.correspondingField]) && (
                                                            <> {row[item.correspondingField]} </>
                                                          )}
                                                      </>
                                                    )}
                                                  </div>
                                                )}
                                              </>
                                            );
                                          })}
                                      </>
                                    )}
                                  </div>

                                  <>
                                    {actionList &&
                                    (allExpanded || !SubComponent) && // when the accordion is expanded or if there is no subComponent show the actions with the content.
                                      actionList.map((acttionItem) => {
                                        return (
                                          <VrsButton
                                            generalButtonColor
                                            onClick={() => this.handleClick(row, acttionItem.title)}
                                            toolTipName={
                                              typeof acttionItem.title !== 'string' ? (
                                                acttionItem.title(row)
                                              ) : (
                                                <FormattedMessage id={acttionItem.title} />
                                              )
                                            }
                                            className={styles.actionButtonMore}
                                            name={acttionItem.name}
                                            authorize={acttionItem.authorize}
                                            {...acttionItem}
                                            title={null}
                                            action={null}
                                            onlyIcon
                                            small
                                            noBg={!acttionItem.noBg}
                                            icon={undefined}
                                          >
                                            {typeof acttionItem.icon !== 'object'
                                              ? acttionItem.icon(row)
                                              : acttionItem.icon}
                                          </VrsButton>
                                        );
                                      })}
                                  </>
                                </div>
                              </AccordionSummary>
                              {accordion && (
                                <AccordionDetails className={styles.userDetail}>
                                  {allExpanded && SubComponent && <>{SubComponent(row)}</>}
                                  {expanded === row[keyField] && SubComponent && <>{SubComponent(row)}</>}

                                  {allButtonListAut && (
                                    <>
                                      {isAuthorized({ name: allButtonListAut }) && (
                                        <>
                                          {allButtonListFilter && (
                                            <div className={styles.AllUserList}>
                                              <div className={styles.ekipinput}>
                                                <input
                                                  id="seachCrime"
                                                  type="text"
                                                  className="input"
                                                  onChange={this.handleSearch}
                                                  placeholder={searchCaption}
                                                />
                                              </div>
                                              <Grid container>
                                                {allButtonListFilter &&
                                                  allButtonListFilter.allItem &&
                                                  allButtonListFilter.allItem.length > 0 &&
                                                  allButtonListFilter.allItem.map((detailItem) => {
                                                    return (
                                                      <Grid item xs={allButtonListFilter.col} className={styles.col}>
                                                        <div className={styles.userListItem} key={detailItem.id}>
                                                          <div className={styles.userListItemIn}>
                                                            <div className={styles.avatar}>
                                                              {detailItem.iconInType &&
                                                              detailItem.iconInType === 'icon' ? (
                                                                <> {detailItem.iconInText}</>
                                                              ) : (
                                                                <>
                                                                  <span> {detailItem.iconInText.split(' ')[0][0]}</span>
                                                                  {detailItem.iconInText.split(' ')[1] && (
                                                                    <span>
                                                                      {detailItem.iconInText.split(' ')[1][0]}
                                                                    </span>
                                                                  )}
                                                                </>
                                                              )}
                                                            </div>
                                                            <div className={styles.text}>
                                                              <span className={styles.name}> {detailItem.name}</span>
                                                              <div className={styles.userRoles}>
                                                                {detailItem.infoList &&
                                                                  detailItem.infoList.map((infoItem) => {
                                                                    return (
                                                                      <>
                                                                        {infoItem.label && (
                                                                          <span className={styles.userRolesName}>
                                                                            {' '}
                                                                            <strong>
                                                                              <FormattedMessage id={infoItem.label} />
                                                                            </strong>
                                                                            {infoItem.value}
                                                                          </span>
                                                                        )}
                                                                      </>
                                                                    );
                                                                  })}
                                                              </div>
                                                            </div>
                                                            <div className={styles.add}>
                                                              <VrsButton
                                                                onClick={() =>
                                                                  this.acordionDetailAddNewItem(row, detailItem)
                                                                }
                                                                className={styles.icon}
                                                                onlyIcon
                                                              >
                                                                <Add />
                                                              </VrsButton>
                                                            </div>
                                                          </div>
                                                          <div className={styles.inputSide}>
                                                            {detailItem.buttonList &&
                                                              detailItem.buttonList.map((buttonItem) => {
                                                                return (
                                                                  <AccordionTableForm
                                                                    type={buttonItem.type}
                                                                    name={buttonItem.name}
                                                                    label={buttonItem.label}
                                                                    row={detailItem}
                                                                    statePass={this.statePass}
                                                                  />
                                                                );
                                                              })}
                                                          </div>
                                                        </div>
                                                      </Grid>
                                                    );
                                                  })}
                                              </Grid>
                                            </div>
                                          )}
                                        </>
                                      )}
                                    </>
                                  )}
                                </AccordionDetails>
                              )}
                            </Accordion>
                          )}
                        </Draggable>
                      );
                    })}
                  {provided.placeholder}
                  {!data ||
                    (!loading && data.length === 0 && (
                      <div className={styles.dataNotFound}>
                        <NotFound />
                      </div>
                    ))}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </div>
      </React.Fragment>
    );
  }
}

export default injectIntl(AccordionTableBody);
