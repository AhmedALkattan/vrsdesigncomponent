import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import { FormattedMessage } from 'react-intl';
import cls from 'classnames';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { IconButton } from '@material-ui/core';
import './VrsTextField.scss';
import { isAuthorized } from '../util/Util';
import styles from './VrsTextField.module.scss';

class VrsTextField extends Component {
  state = {
    changed: false,
    showPassword: false
  };

  handleChange = (e) => {
    const { onChange, isNumber } = this.props;

    if (isNumber) {
      const numberReg = new RegExp('^[0-9\\.]+$');
      const isValid = numberReg.test(e.currentTarget.value);
      if (isValid || e.currentTarget.value === '') onChange(e.currentTarget.value);
    }

    if (onChange && !isNumber) onChange(e.currentTarget.value);
    this.setState({
      changed: true
    });
  };

  handleClickShowPassword = (showPassword) => {
    this.setState({
      showPassword
    });
  };

  render() {
    const {
      id,
      type,
      label,
      value,
      placeholder,
      disabled,
      multiline,
      rowsMax,
      rows,
      style,
      children,
      formControlStyle,
      required,
      accept,
      isMultiFile,
      onBlur,
      variant,
      childRef,
      shouldAuthorize,
      onChange,
      maxLen,
      margin,
      noLabel,
      centered,
      hasCustomColoredText,
      customColor,
      ...otherProps
    } = this.props;

    const { changed, showPassword } = this.state;

    const InputLabelProps = {
      classes: {
        root: 'text-field-label',
        shrink: 'text-field-shrink',
        asterisk: 'text-field-asterisk'
      }
    };

    const InputProps = {
      className: hasCustomColoredText && 'customColoredText',
      style: {
        color: customColor
      },
      classes: {
        root: 'text-field-input-root text-field-input',
        underline: 'text-field-underline'
      }
    };

    // material ui 0 olanları yazmıyor mecbur böyle bi koşul yazzdık.
    let values;

    if (value === 0 && typeof value === 'number') {
      values = '0';
    } else {
      values = value;
    }

    return (
      (!shouldAuthorize || isAuthorized(id)) && (
        <FormControl style={formControlStyle} margin={margin || 'normal'} fullWidth autoComplete="off">
          {noLabel ? (
            <Input
              name={id}
              id={id}
              inputRef={childRef}
              type={showPassword && type === 'password' ? 'text' : type}
              required={required}
              label={label}
              value={values || ''}
              error={required && changed && !value}
              placeholder={placeholder}
              onChange={this.handleChange}
              onBlur={onBlur ? (e) => onBlur(e.currentTarget.value) : null}
              disabled={disabled}
              className={cls(`${styles.textField} text-field`, { [styles.textFieldCenter]: centered })}
              multiline={multiline}
              rowsMax={rowsMax}
              rows={rows}
              variant={variant || 'standard'}
              autoComplete="off"
              // shouldUpdate={this.props.shouldUpdate || false}
              inputProps={{ 'aria-label': 'description' }}
              InputLabelProps={{ ...InputLabelProps }}
              style={style}
              {...otherProps}
            />
          ) : (
            <TextField
              name={id}
              id={id}
              inputRef={childRef}
              type={showPassword && type === 'password' ? 'text' : type}
              required={required}
              label={label}
              value={values || ''}
              error={required && changed && !value && value !== ''}
              placeholder={placeholder}
              onChange={this.handleChange}
              onBlur={onBlur ? (e) => onBlur(e.currentTarget.value) : null}
              disabled={disabled}
              className={cls(`${styles.textField} text-field`, { [styles.textFieldCenter]: centered })}
              multiline={multiline}
              rowsMax={rowsMax}
              rows={rows}
              variant={variant || 'standard'}
              autoComplete="off"
              // shouldUpdate={this.props.shouldUpdate || false}
              InputLabelProps={{ ...InputLabelProps }}
              InputProps={{ ...InputProps }}
              style={style}
              {...otherProps}
            />
          )}

          {children}
          {required && changed && !value && value !== '' && (
            <FormHelperText className="validation-error-message">
              <FormattedMessage id="isRequired" />
            </FormHelperText>
          )}

          {type === 'password' && (
            <div className={styles.visibilityButton}>
              {!showPassword ? (
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => this.handleClickShowPassword(true)}
                  edge="end"
                >
                  <Visibility />
                </IconButton>
              ) : (
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => this.handleClickShowPassword(false)}
                  edge="end"
                >
                  <VisibilityOff />
                </IconButton>
              )}
            </div>
          )}
        </FormControl>
      )
    );
  }
}

export default VrsTextField;
