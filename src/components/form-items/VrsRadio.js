import React from 'react';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import styles from './VrsRadio.module.scss';

const VrsRadio = ({ nameHolder, name, value, string, onChange, label, placement }) => {
  return (
    <div className={styles.radioContainer}>
      <FormControlLabel
        value={value}
        classes={{
          // root: cls(styles.checkboxLalbelContainer, { [checkboxLalbelContainerPlace]: placement }),
          root: styles.radioRoot,
          label: styles.radioLabel
        }}
        control={
          <Radio
            checked={nameHolder === value || false}
            onChange={(e) => onChange(name)(string ? e.target.value.toString() : parseInt(e.target.value, 10))}
            value={value}
            name={name}
            aria-label={name}
            label={label}
            classes={{
              root: styles.radioInput
            }}
          />
        }
        label={label}
        labelPlacement={placement || 'end'}
      />
    </div>
  );
};

export const InlineRadio = ({ value, label, placement }) => {
  return <FormControlLabel value={value} control={<Radio />} label={label} labelPlacement={placement || 'end'} />;
};

export default VrsRadio;
