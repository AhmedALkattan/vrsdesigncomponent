/* eslint-disable react/no-did-update-set-state */
import React, { Component } from 'react';
import { CheckCircleOutline, CheckCircle } from '@material-ui/icons';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import cls from 'classnames';
import { FormattedMessage } from 'react-intl';
import styles from './VrsCheckbox.module.scss';
import { isAuthorized } from '../util/Util';

class VrsCheckbox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stateChecked: false
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.stateChecked !== nextProps.checked) {
      return { stateChecked: nextProps.checked };
    }
    return null;
  }

  componentDidMount() {
    const { checked } = this.props;
    const { stateChecked } = this.state;

    if (checked !== stateChecked) {
      this.setState({
        stateChecked: checked
      });
    }
  }

  handleClick = (isChecked) => {
    const { onChange } = this.props;
    if (onChange) {
      this.setState((oldState) => ({
        stateChecked: !oldState.stateChecked
      }));
      onChange(!isChecked);
    } else {
      this.setState((oldState) => ({
        stateChecked: !oldState.stateChecked
      }));
    }
  };

  render() {
    const {
      id,
      authorize,
      isRight,
      label,
      value,
      inline,
      className,
      checkboxClass,
      checkedIcon,
      radial,
      icon,
      disabled,
      placement,
      autoWidth
    } = this.props;
    const { stateChecked } = this.state;
    const {
      checkboxBlock,
      checkbox,
      checked,
      labelRight,
      checkboxBlockLabel,
      checkboxLalbelContainerPlace,
      checkboxBlockAuto
    } = styles;

    const element = (
      <div
        className={cls(className, {
          [checkboxBlock]: !inline,
          [checkboxBlockAuto]: autoWidth,
          [labelRight]: isRight,
          [checkboxBlockLabel]: label
        })}
      >
        <FormControlLabel
          classes={{
            root: cls(styles.checkboxLalbelContainer, { [checkboxLalbelContainerPlace]: placement }),
            label: styles.checkboxLalbel
          }}
          control={
            <Checkbox
              checked={value || stateChecked}
              value={value || undefined}
              classes={{ checked }}
              onClick={() => this.handleClick(value || stateChecked)}
              className={cls(checkbox, checkboxClass)}
              checkedIcon={radial ? <CheckCircle /> : checkedIcon}
              icon={radial ? <CheckCircleOutline /> : icon}
              disabled={disabled}
            />
          }
          label={label && <FormattedMessage defaultMessage={label} id={label} />}
          labelPlacement={label ? placement || 'start' : 'end'}
        />
      </div>
    );

    if (authorize) {
      return isAuthorized({ id }) ? element : null;
    }
    return element;
  }
}

export default VrsCheckbox;
