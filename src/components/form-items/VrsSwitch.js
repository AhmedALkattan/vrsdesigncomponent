/* eslint-disable react/no-did-update-set-state */
import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import Switch from '@material-ui/core/Switch';
import cls from 'classnames';
import styles from './VrsSwitch.module.scss';
import { isAuthorized } from '../util/Util';

class VrsSwitch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stateValue: false
    };
  }

  componentDidMount() {
    const { checked } = this.props;
    const { stateValue } = this.state;

    if (checked !== stateValue) {
      this.setState({
        stateValue: checked
      });
    }
  }

  componentDidUpdate() {
    const { checked } = this.props;
    const { stateValue } = this.state;

    if (checked !== stateValue) {
      this.setState({
        stateValue: checked
      });
    }
  }

  handleChange = (value) => {
    const { onChange } = this.props;
    onChange(value);
    this.setState({
      stateValue: value
    });
  };

  render() {
    const { container, barColor, checkkedColor, switchContainer, spanColor, switchRoot, switchRootDanger } = styles;
    const { id, shouldBeAuthorized, checked, value, label, twoSide, onChange, icon, color, ...otherProps } = this.props;
    const { stateValue } = this.state;

    return (
      (!shouldBeAuthorized || isAuthorized(id)) && (
        <div className={`${container} vrsSwichContainer`} {...otherProps}>
          {label && (
            <button
              onClick={() => this.handleChange(false)}
              className={cls(styles.labelButton, { [styles.active]: !stateValue })}
            >
              {label && <FormattedMessage id={twoSide ? label.left : label} />}
              {icon && icon.left}
            </button>
          )}
          <Switch
            classes={{
              track: barColor,

              root: cls(switchRoot, { [switchRootDanger]: color === 'danger' }),
              switchBase: switchContainer,
              thumb: spanColor,
              checked: checkkedColor

              // row: swichprimary,
            }}
            checked={stateValue}
            onChange={(e, value) => this.handleChange(value)}
            value={value}
            disableRipple
          />
          {label && label.right && (
            <button
              onClick={() => this.handleChange(true)}
              className={cls(styles.labelButton, { [styles.active]: stateValue })}
            >
              {label && <FormattedMessage id={twoSide ? label.right : label} />}
              {icon && icon.right}
            </button>
          )}
        </div>
      )
    );
  }
}

export default VrsSwitch;
