import React from 'react';
import cls from 'classnames';
import { FormattedMessage } from 'react-intl';
import { SnackbarContent, Snackbar } from '@material-ui/core';
import styles from './VrsSnackbar.module.scss';

const VrsSnackbar = ({ open, message, onClose, formatMessage, action, variant, currentLanguage }) => {
  const anchorOrigin = { vertical: 'bottom', horizontal: currentLanguage === 'ar' ? 'left' : 'right' };

  const variants = {
    danger: styles.danger,
    warning: styles.warning,
    success: styles.success
  };

  return (
    <Snackbar anchorOrigin={anchorOrigin} open={open} autoHideDuration={3000} onClose={onClose}>
      <SnackbarContent
        className={cls(styles.common, variants[variant])}
        action={action}
        message={formatMessage ? <FormattedMessage id={message} /> : message}
      />
    </Snackbar>
  );
};

export default VrsSnackbar;
