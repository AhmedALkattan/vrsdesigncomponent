import React, { Component } from 'react';
import cls from 'classnames';
import styles from './Skeleton.module.scss';

class Skeleton extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { children, width, height, container, otherProps, formElement } = this.props;

    return (
      <React.Fragment>
        <div
          {...otherProps}
          className={cls(
            styles.skeleton,
            { [styles.skeletonWrapper]: container },
            { [styles.skeletonItem]: !container },
            { [styles.skeletonFormItem]: formElement }
          )}
          style={{ width: `${width}`, height: `${height}` }}
        >
          {children}
        </div>
      </React.Fragment>
    );
  }
}

export default Skeleton;
