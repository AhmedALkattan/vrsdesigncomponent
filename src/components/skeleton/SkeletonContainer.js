import React, { Component } from 'react';
import cls from 'classnames';
import styles from './SkeletonContainer.module.scss';

class SkeletonContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { children, direction, width, height, main, container } = this.props;

    return (
      <React.Fragment>
        <div
          className={cls(
            styles.skeletonContainer,
            { [styles.skeletonContainerHorizontal]: direction === 'horizontal' },
            { [styles.skeletonContainerMain]: main },
            { [styles.skeletonContainerContainer]: container }
          )}
          style={{ width: `${width}`, height: `${height}` }}
        >
          {children}
        </div>
      </React.Fragment>
    );
  }
}

export default SkeletonContainer;
