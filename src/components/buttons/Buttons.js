import React, { Component } from 'react';
import cls from 'classnames';
import { Tooltip, Menu, MenuItem } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import styles from './Buttons.module.scss';
import { isAuthorized } from '../util/Util';

class VrsButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null
    };
  }

  handleClick = (event) => {
    const { dropdown, onClick } = this.props;
    if (dropdown) {
      this.setState({ anchorEl: event.currentTarget });
    } else {
      onClick(event);
    }
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const {
      authorize,
      authorizationsList,
      inlineStyle,
      id,
      children,
      className,
      icon,
      danger,
      link,
      hasBg,
      light,
      fullWidth,
      outlined,
      info,
      success,
      warning,
      small,
      large,
      noBorder,
      label,
      secondary,
      onlyIcon,
      toolTipName,
      textColor,
      noBg,
      generalButtonColor,
      circle,
      grey,
      active,
      xs,
      onClick,
      dropdown,
      item,
      disabled,
      linkColor,
      ...otherProps
    } = this.props;

    const Tag = link ? 'a' : 'button';
    const accents = [
      styles.button,
      {
        [styles.danger]: danger,
        [styles.hasBg]: hasBg,
        [styles.light]: light,
        light,
        [styles.fullWidth]: fullWidth,
        [styles.onlyIcon]: !children,
        [styles.onlyIcon]: onlyIcon,
        [styles.outlined]: outlined,
        [styles.info]: info,
        [styles.success]: success,
        [styles.warning]: warning,
        [styles.large]: large,
        [styles.small]: small,
        [styles.noBorder]: noBorder,
        [styles.secondary]: secondary,
        [styles.justTextColor]: textColor,
        [styles.noBg]: noBg,
        [styles.generalButtonColor]: generalButtonColor,
        [styles.circle]: circle,
        [styles.grey]: grey,
        [styles.active]: active,
        [styles.xs]: xs,
        [styles.linkColor]: linkColor
      }
    ];
    const { anchorEl } = this.state;
    const classes = cls(className, inlineStyle ? '' : accents);
    const element = (
      <>
        <Tooltip id="tooltip-icon" placement="bottom" title={(!disabled && toolTipName) || ''}>
          <Tag onClick={this.handleClick} {...otherProps} className={classes} disabled={disabled}>
            {icon && icon}
            {children && children}
            {label && label}
          </Tag>
        </Tooltip>

        {dropdown && item && item.dropdown && (
          <Menu id="lock-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={this.handleClose}>
            {item.dropdown.map((menuItem, i) => (
              <MenuItem key={i.toString()} onClick={menuItem.action}>
                <FormattedMessage id={menuItem.name} />
              </MenuItem>
            ))}
          </Menu>
        )}
      </>
    );

    if (authorize) {
      return isAuthorized({ id }, authorizationsList) ? element : null;
    }
    return element;
  }
}

export default VrsButton;
