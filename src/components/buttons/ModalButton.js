import React from 'react';
import { FormattedMessage } from 'react-intl';
import VrsButton from './Buttons';
import { isAuthorized } from '../util/Util';

const Button = ({ id, label, onClick, type, disabled, danger, ...otherProps }) => {
  return (
    <VrsButton onClick={onClick} type={type || 'button'} disabled={disabled} danger={danger} {...otherProps}>
      <FormattedMessage id={label} />
    </VrsButton>
  );
};

const ModalButton = ({ authorize, id, ...otherProps }) => {
  if (authorize) {
    return isAuthorized({ id }) ? <Button {...otherProps} /> : null;
  }
  return <Button {...otherProps} />;
};

export default ModalButton;
