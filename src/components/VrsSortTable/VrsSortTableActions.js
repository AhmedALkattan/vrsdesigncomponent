/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import cls from 'classnames';
import { FormattedMessage } from 'react-intl';
import styles from './VrsSortTableTd.module.scss';
import VrsButton from '../buttons/Buttons';

class VrsSortTableActions extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleClick = (row, title) => {
    const { actionList } = this.props;
    let findButton;
    if (row.rowActions) {
      findButton = row.rowActions.find((o) => o.title === title);
    } else {
      findButton = actionList.find((o) => o.title === title);
    }

    findButton.action(row, title);
  };

  render() {
    const { actionList, row } = this.props;
    return (
      <>
        <div
          className={cls(styles.stTd, styles.stTdActions)}
          // style={{ width: `135px`, flex: `135 0 auto`, maxWidth: '135px' }}
        >
          {actionList &&
            actionList.map((acttionItem) => {
              return (
                <VrsButton
                  generalButtonColor
                  onClick={() => this.handleClick(row, acttionItem.title)}
                  toolTipName={
                    typeof acttionItem.title !== 'string' ? (
                      acttionItem.title(row)
                    ) : (
                      <FormattedMessage id={acttionItem.title} />
                    )
                  }
                  className={styles.actionButtonMore}
                  name={acttionItem.name}
                  authorize={acttionItem.authorize}
                  {...acttionItem}
                  onlyIcon
                  small
                  noBg={!acttionItem.noBg}
                  icon={undefined}
                >
                  {typeof acttionItem.icon !== 'object' ? acttionItem.icon(row) : acttionItem.icon}
                </VrsButton>
              );
            })}
        </div>
      </>
    );
  }
}

export default VrsSortTableActions;
