import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import cls from 'classnames';
import styles from './VrsSortTableHead.module.scss';

class VrsSortTableHead extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { columns, columnsHidden } = this.props;
    return (
      <>
        <div className={styles.headerWrapper}>
          {!columnsHidden &&
            columns &&
            columns.map((item, i) => {
              return (
                <>
                  {item.isVisible && (
                    <div
                      className={cls(styles.headerItem, { [styles.headerItemCenter]: item.alignmentIsMiddle })}
                      style={item.width ? { width: `${item.width}px` } : { width: '150px' }}
                      key={i.toString()}
                    >
                      {item.columnCaption && (
                        <FormattedMessage defaultMessage={item.columnCaption} id={item.columnCaption} />
                      )}
                    </div>
                  )}
                </>
              );
            })}
        </div>
      </>
    );
  }
}

export default VrsSortTableHead;
