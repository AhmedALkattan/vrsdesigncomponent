/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import cls from 'classnames';
import styles from './VrsSortTableTd.module.scss';

class VrsSortTableTd extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { item, row } = this.props;
    return (
      <>
        {item.isVisible && (
          <div
            className={cls(styles.stTd, {
              [styles.stTdCenter]: item.alignmentIsMiddle
            })}
            style={
              item.width !== 'auto'
                ? { width: `${item.width}px`, flex: `${item.width} 0 auto`, maxWidth: `${item.width}px` }
                : { width: `120px`, flex: `1 1 120px` }
            }
          >
            {!row.rowEdit && item.Cell ? (
              <div className={styles.stTdChild}>{item.Cell(row)}</div>
            ) : (
              <>
                {row[item.correspondingField] && !Array.isArray(row[item.correspondingField]) && (
                  <div className={styles.stTdChild}> {row[item.correspondingField]} </div>
                )}
              </>
            )}
          </div>
        )}
      </>
    );
  }
}

export default VrsSortTableTd;
