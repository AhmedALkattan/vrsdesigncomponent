/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import cls from 'classnames';
import styles from './VrsSortTableRow.module.scss';
import VrsSortTableTd from './VrsSortTableTd';
import VrsSortTableActions from './VrsSortTableActions';

class VrsSortTableRow extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onRowDblClick = (row) => {
    const { onRowDblClick } = this.props;
    onRowDblClick(row);
  };

  render() {
    const { columns, row, actionList, groupStartPackage } = this.props;
    return (
      <>
        <div
          className={cls(styles.tableRow, { [styles.group]: groupStartPackage })}
          onDoubleClick={() => this.onRowDblClick(row)}
        >
          {columns &&
            columns.map((item) => {
              return (
                <>
                  <VrsSortTableTd item={item} row={row} />
                </>
              );
            })}
          <VrsSortTableActions actionList={actionList} row={row} />
        </div>
      </>
    );
  }
}

export default VrsSortTableRow;
