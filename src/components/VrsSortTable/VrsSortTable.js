/* eslint-disable import/imports-first */
import React, { Component } from 'react';
import SortableTree, { toggleExpandedForAll } from 'react-sortable-tree';
import cls from 'classnames';
import { DragIndicator } from '@material-ui/icons';
import VrsSortTableHead from './VrsSortTableHead';
import VrsSortTableRow from './VrsSortTableRow';
import styles from './VrsSortTable.module.scss';
import 'react-sortable-tree/style.css';
import './VrsSortTable.scss';
import Loader from '../loader/Loader';

class VrsSortTable extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { data } = this.props;
    this.setState({
      treeData: data
    });
    this.expand(true, data);
  }

  componentDidUpdate(prevProps) {
    const { data } = this.props;
    if (prevProps.data !== data) {
      this.expand(true, data);
    }
  }

  onChangeData = (data) => {
    const { onChangeData } = this.props;

    onChangeData(data);
  };

  onRowDblClick = (row) => {
    const { onRowDblClick } = this.props;
    onRowDblClick(row);
  };

  expand = (expanded, data) => {
    this.setState({
      treeData: toggleExpandedForAll({
        treeData: data,
        expanded
      })
    });
  };

  orderLayers = (orderLayerData) => {
    const { orderLayers } = this.props;
    orderLayers(orderLayerData);
  };

  render() {
    const { columns, columnsHidden, rowDirection, loading, actionList, canDrag, groupStartPackage } = this.props;
    const { treeData } = this.state;

    return (
      <div className={cls(styles.sortableTreeContainer, { [styles.sortableTreeContainerNotDrag]: !canDrag })}>
        {loading && <Loader loading={loading} reset />}
        <VrsSortTableHead columns={columns} columnsHidden={columnsHidden} />
        <SortableTree
          treeData={treeData}
          rowDirection={rowDirection}
          rowHeight={50}
          onChange={(treeData) => this.onChangeData(treeData)}
          toggleExpandedForAll
          onMoveNode={this.orderLayers}
          canDrag={canDrag}
          isVirtualized
          generateNodeProps={({ node }) => ({
            title: (
              <>
                <div className={styles.moveIcon}>
                  <DragIndicator />
                </div>
                <VrsSortTableRow
                  actionList={actionList}
                  columns={columns}
                  row={node}
                  groupStartPackage={node[groupStartPackage]}
                  onRowDblClick={(node) => this.onRowDblClick(node)}
                />
              </>
            )
          })}
        />
      </div>
    );
  }
}

export default VrsSortTable;
