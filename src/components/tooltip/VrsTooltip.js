import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import cls from 'classnames';
import styles from './VrsTooltip.module.scss';

class VrsTooltip extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showTooltip: false
    };

    this.button = React.createRef();
    this.tooltip = React.createRef();
  }

  handleHover = (e, show) => {
    this.setState({
      showTooltip: show
    });

    const elementPositions = this.button.current.getBoundingClientRect();
    this.setPositions(elementPositions);
  };

  setPositions = ({ top, left }) => {
    this.tooltip.current.style.top = `${top}px`;
    this.tooltip.current.style.left = `${left}px`;
  };

  render() {
    const { isDisabled, children, title, onClick, text, light, type, list, icon, ...otherProps } = this.props;
    const { showTooltip } = this.state;
    return (
      <React.Fragment>
        {title && (
          <>
            <button
              ref={this.button}
              type="button"
              className={styles.actionButton}
              disabled={isDisabled}
              onMouseEnter={(e) => this.handleHover(e, true)}
              onMouseLeave={(e) => this.handleHover(e, false)}
              onClick={onClick}
              {...otherProps}
            >
              {children}
            </button>
            <div
              ref={this.tooltip}
              className={cls(styles.tooltip, {
                [styles.tooltipLight]: type === 'light'
              })}
              style={{ display: showTooltip ? 'flex' : 'none' }}
            >
              <div className={styles.header}>
                {icon && <span className={styles.icon}>{icon}</span>}

                <span className={styles.title}>
                  <FormattedMessage id={title} />
                </span>
              </div>
              {list && (
                <div className={styles.content}>
                  {list ? (
                    <>
                      {list.map((item) => {
                        return <div className={styles.item}>{item.defCertificateTypes.label}</div>;
                      })}
                    </>
                  ) : (
                    <>boş</>
                  )}
                </div>
              )}
            </div>
          </>
        )}
      </React.Fragment>
    );
  }
}

export default VrsTooltip;
