import React, { Component } from 'react';
import Drawer from '@material-ui/core/Drawer';
import { Check, Close, FilterList } from '@material-ui/icons';
import { FormattedMessage } from 'react-intl';
import cls from 'classnames';
import styles from './FilterMenu.module.scss';
import VrsButton from '../buttons/Buttons';
import './LightFields.scss';
import '../styles/global.scss';

class FilterMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      changedItem: [],
      values: {}
    };
  }

  handleToggle = () => {
    this.setState((oldState) => ({
      open: !oldState.open
    }));
  };

  statePass = (name, value) => {
    this.setState((oldState) => {
      let isExist = false;

      oldState.changedItem.forEach((item) => {
        if (item === name) {
          isExist = true;
        } else {
          isExist = false;
        }
      });

      if (isExist) {
        return {
          values: {
            ...oldState.values,
            [name]: value
          }
        };
      }
      return {
        values: {
          ...oldState.values,
          [name]: value
        },
        changedItem: [...oldState.changedItem, name]
      };
    });
  };

  onClear = () => {
    const { changedItem } = this.state;

    changedItem.forEach((item) =>
      this.setState({
        values: {
          [item]: ''
        }
      })
    );
  };

  onPressEnter = (e) => {
    const { values } = this.state;
    const { onApply } = this.props;
    if (e.keyCode === 13) {
      onApply(values);
    }
    return undefined;
  };

  render() {
    const { open, values } = this.state;
    const { content, inline, name, title, onApply, setSnackbar } = this.props;
    const Content = content;

    return (
      <React.Fragment>
        <VrsButton
          className={cls(styles.topNavIcon)}
          secondary
          onlyIcon
          small
          type="button"
          onClick={this.handleToggle}
        >
          <FilterList />
          {inline && (
            <div className="top-nav-name">
              <FormattedMessage id={name} />
            </div>
          )}
        </VrsButton>
        {/* <button className={cls(styles.topNavIcon)} type="button" onClick={this.handleToggle}>
          <FilterList />
          {inline && (
            <div className="top-nav-name">
              <FormattedMessage id={name} />
            </div>
          )}
        </button> */}

        <Drawer
          open={open}
          anchor="right"
          onClose={this.handleToggle}
          classes={{
            paper: cls(styles.filterDetailedForm, 'filterDetailedForm')
          }}
          width={300}
          onKeyDown={this.onPressEnter}
        >
          <div className={cls(styles.drawerContainer, styles.drawerFilter, 'filterSide')}>
            <div className={cls(styles.drawerHeader, 'filterHeader')}>
              <FormattedMessage id={title} />
            </div>
            <div className={cls(styles.drawerBody, 'light-fields')}>
              {content ? <Content setSnackbar={setSnackbar} statePass={this.statePass} values={values} /> : null}
            </div>
            <div className={cls(styles.drawerFooter, 'drawerFooter')}>
              <VrsButton className="clear" icon={<Close />} light onClick={this.onClear}>
                <FormattedMessage id="clear" />
              </VrsButton>
              {/* <div className={cls(styles.divider, 'divider')} /> */}
              <VrsButton className="primary" icon={<Check />} hasBg onClick={() => onApply(values)}>
                <FormattedMessage id="apply" />
              </VrsButton>
            </div>
          </div>
        </Drawer>
      </React.Fragment>
    );
  }
}
export default FilterMenu;
