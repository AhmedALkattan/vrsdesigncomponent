import React from 'react';
import { FormattedMessage } from 'react-intl';

const BarLegend = ({ payload, iconSize }) => {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      {payload.map((entry, i) => (
        <React.Fragment key={i.toString()}>
          <div
            className="circle"
            style={{
              width: `${iconSize}px`,
              height: `${iconSize}px`,
              borderRadius: '50%',
              background: entry.color,
              margin: '0 0.5em'
            }}
          />
          <span style={{ margin: '0 .5em', color: entry.color }}>
            <FormattedMessage id={entry.value} />
          </span>
        </React.Fragment>
      ))}
    </div>
  );
};

export default BarLegend;
