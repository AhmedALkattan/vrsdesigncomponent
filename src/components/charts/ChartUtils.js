export default class ChartUtils {
  static getMaxBarData(data, dataKey) {
    let max = 0;
    data.forEach((item) => {
      dataKey.forEach((key) => {
        max = Math.max(item[key.keyName], max);
      });
    });
    return max;
  }

  static axisWidth(number) {
    return Math.max(60, 30 + Math.log10(number) * 10);
  }
}
