import React, { Component } from 'react';
import { Cell, Pie, PieChart, ResponsiveContainer, Tooltip } from 'recharts';
// import PieActiveShape from './PieActiveShape';

class VrsPieChart extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { data, chartHeight, dataColor, styles, onPieClick } = this.props;

    let pieColor;
    if (dataColor && dataColor.length !== 0) {
      pieColor = dataColor;
    } else {
      pieColor = data.pieData;
    }

    const CustomTooltip = ({ active, payload }) => {
      if (active) {
        return (
          <div className={`${styles.customTooltip} box`} style={{ boxShadowColor: payload[0].payload.color }}>
            <span className={styles.title}>{payload[0].name}</span>
            <span className={styles.count} style={{ color: payload[0].payload.color }}>
              ({payload[0].value})
            </span>
          </div>
        );
      }

      return null;
    };

    return (
      <div className={styles.pieChartContainer}>
        <div className={styles.customPieLegendContainer}>
          {data &&
            pieColor &&
            pieColor.map((item, i) => {
              return (
                <div className={styles.customPieLegend} onClick={() => onPieClick(null, i)} key={i}>
                  <div className={styles.square} style={{ background: item.color }} />
                  <div className={styles.title}>
                    <p> {item.name}</p>
                    <span>{item.count}</span>
                  </div>
                </div>
              );
            })}
        </div>
        <div className={styles.customPieCartContainer}>
          <ResponsiveContainer height={chartHeight || 290}>
            <PieChart margin={{ bottom: 20 }}>
              <Pie
                isAnimationActive
                data={data.pieData}
                dataKey={data.pieDataKey}
                cx="50%"
                cy="50%"
                innerRadius="50%"
                outerRadius="70%"
                onClick={onPieClick}
              >
                {pieColor.map((item, index) => (
                  // <Cell key={index.toString()} fill={item.color} onMouseEnter={() => this.onPieEnter(index)} />
                  <Cell key={index.toString()} fill={item.color} />
                ))}
              </Pie>
              <Tooltip content={<CustomTooltip />} />
              {/* <Legend verticalAlign="left" layout="vertical" /> */}
            </PieChart>
          </ResponsiveContainer>
        </div>
      </div>
    );
  }
}

export default VrsPieChart;
