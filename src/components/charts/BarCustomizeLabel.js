import React from 'react';
import styles from './Charts.module.scss';

const BarCustomizeLabel = (props) => {
  const { x, y, width, value } = props;
  const radius = 18;

  return (
    <>
      <g className={styles.customizeLabel}>
        {/* <circle cx={x + width / 2} cy={y - radius} r={radius} fill="#fff" stroke="#986f2f" strokeWidth="1" /> */}
        <text x={x + width / 2} y={y - radius} fill="#986f2f" textAnchor="middle" dominantBaseline="middle">
          {value}
        </text>
      </g>
    </>
  );
};

export default BarCustomizeLabel;
