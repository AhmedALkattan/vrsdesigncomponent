import React, { Component } from 'react';
import { Cell, Pie, PieChart, Sector, ResponsiveContainer } from 'recharts';
import { DefaultTheme } from './ChartColors';

const data = [
  { name: 'data1', count: 0, color: DefaultTheme.chart_color_4 },
  { name: 'data2', count: 0, color: DefaultTheme.chart_color_6 },
  { name: 'data3', count: 0, color: DefaultTheme.chart_color_5 }
];

const renderActiveShape = (props) => {
  const {
    cx,
    cy,
    // midAngle,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload,
    // percent,
    count
  } = props;

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill="#333">
        {`${payload.name}  ${count}`}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius * 1.2}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
    </g>
  );
};

class VrsGaugeChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeIndex: 0
    };
  }

  onPieEnter = (index) => {
    this.setState({
      activeIndex: index
    });
  };

  render() {
    const { activeIndex } = this.state;
    return (
      <div className="chart-container">
        <span className="chart-title">Data Distribution by Something Else</span>
        <span className="chart-title-center">{`Total: ${data[0].count + data[1].count + data[2].count}`}</span>
        <div className="chart">
          <ResponsiveContainer>
            <PieChart>
              <Pie
                activeIndex={activeIndex}
                activeShape={renderActiveShape}
                isAnimationActive
                data={data}
                dataKey="count"
                cx="50%"
                cy="70%"
                innerRadius="50%"
                outerRadius="100%"
                startAngle={180}
                endAngle={0}
              >
                {data.map((item, index) => (
                  <Cell key={item} fill={item.color} onMouseEnter={() => this.onPieEnter(index)} />
                ))}
              </Pie>
            </PieChart>
          </ResponsiveContainer>
        </div>
      </div>
    );
  }
}

export default VrsGaugeChart;
